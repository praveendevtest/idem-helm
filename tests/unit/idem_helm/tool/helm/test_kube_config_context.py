import copy
import os

import dict_tools


def test_get_kube_config_and_context_failed(mock_hub, hub, ctx):
    # Assert get_kube_config_and_context raises exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy.acct["kube_config_path"] = None
    ctx_copy.acct["context"] = None
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    try:
        hub.tool.helm.command_utils.get_kube_config_and_context(ctx_copy)
        assert False
    except Exception as exc:
        assert exc.message == "kube_config_path need to be set"


def test_get_kube_config_and_context_from_evn(mock_hub, hub, ctx):
    # Assert get_kube_config_and_context raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy.acct["kube_config_path"] = None
    ctx_copy.acct["context"] = None
    os.environ["KUBE_CONFIG_PATH"] = ctx.acct["kube_config_path"]
    os.environ["KUBE_CTX"] = ctx.acct["context"]
    try:
        hub.tool.helm.command_utils.get_kube_config_and_context(ctx_copy)
    except Exception as exc:
        assert False, f"get_kube_config_and_context raised an exception {exc}"


def test_get_kube_config_current_context(mock_hub, hub, ctx):
    # Assert get_kube_config_and_context raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy.acct["kube_config_path"] = None
    ctx_copy.acct["context"] = None
    os.environ["KUBE_CONFIG_PATH"] = ctx.acct["kube_config_path"]
    os.environ.pop("KUBE_CTX", None)
    try:
        commands = ["install", "release-1", "chart-1"]
        kvflags = {
            "namespace": "kube-system",
            "repo": "https://charts.bitnami.com/bitnami",
        }
        cmd = hub.tool.helm.command_utils.prepare_command(
            ctx_copy, commands, None, kvflags
        )
        assert "--kubeconfig" in cmd
        assert "--kube-context" not in cmd
    except Exception as exc:
        assert False, f"get_kube_config_and_context raised an exception {exc}"


def test_get_kube_config_and_context_from_extras(mock_hub, hub, ctx):
    # Assert get_kube_config_and_context raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy.acct["kube_config_path"] = None
    ctx_copy.acct["context"] = None
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "idem": {"acct_profile": "default"},
            "acct": {
                "extras": {
                    "helm": {
                        "default": {
                            "kube_config_path": ctx.acct["kube_config_path"],
                            "context": ctx.acct["context"],
                        }
                    }
                }
            },
        }
    )
    try:
        hub.tool.helm.command_utils.get_kube_config_and_context(ctx_copy)
    except Exception as exc:
        assert False, f"get_kube_config_and_context raised an exception {exc}"


def test_get_kube_config_and_context_from_ctx_acct(mock_hub, hub, ctx):
    # Assert get_kube_config_and_context raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy.acct["kube_config_path"] = ctx.acct["kube_config_path"]
    ctx_copy.acct["context"] = ctx.acct["context"]
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "idem": {"acct_profile": "default"},
            "acct": {
                "extras": {
                    "helm": {
                        "other-profile": {  # different profile name
                            "kube_config_path": ctx.acct["kube_config_path"],
                            "context": ctx.acct["context"],
                        }
                    }
                }
            },
        }
    )
    try:
        hub.tool.helm.command_utils.get_kube_config_and_context(ctx_copy)
    except Exception as exc:
        assert False, f"get_kube_config_and_context raised an exception {exc}"
