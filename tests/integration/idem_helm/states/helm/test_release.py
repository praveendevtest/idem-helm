import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_release(hub, ctx):
    helm_release_temp_name = "idem-test-" + str(uuid.uuid4())
    repository = "https://charts.bitnami.com/bitnami"
    chart = "redis"
    namespace = "kube-system"
    values = {
        "image": {"pullPolicy": "IfNotPresent"},
    }

    # create helm_release with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.helm.release.present(
        test_ctx,
        name=helm_release_temp_name,
        repository=repository,
        chart=chart,
        namespace=namespace,
        values=values,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create helm.release '{helm_release_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        values,
        ret.get("new_state"),
        "resource_id_known_after_present",
    )

    # create real helm_release
    ret = await hub.states.helm.release.present(
        ctx,
        name=helm_release_temp_name,
        repository=repository,
        chart=chart,
        namespace=namespace,
        values=values,
    )
    assert ret["result"], ret["comment"]
    assert f"Created helm.release '{helm_release_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        values,
        ret.get("new_state"),
    )

    resource_id = ret.get("new_state").get("resource_id")

    # Describe helm_release
    describe_ret = await hub.states.helm.release.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "helm.release.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("helm.release.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        values,
        described_resource_map,
    )

    # update helm_release with test flag
    updated_values = copy.deepcopy(values)
    updated_values["image"]["pullPolicy"] = "Always"
    ret = await hub.states.helm.release.present(
        test_ctx,
        name=helm_release_temp_name,
        repository=repository,
        chart=chart,
        namespace=namespace,
        values=updated_values,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update helm.release '{helm_release_temp_name}'" in ret["comment"]
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        values,
        ret.get("old_state"),
    )
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        updated_values,
        ret.get("new_state"),
    )

    # real update helm_release
    ret = await hub.states.helm.release.present(
        ctx,
        name=helm_release_temp_name,
        repository=repository,
        chart=chart,
        namespace=namespace,
        values=updated_values,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated helm.release '{helm_release_temp_name}'" in ret["comment"]
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        values,
        ret.get("old_state"),
    )
    assert_release(
        helm_release_temp_name,
        chart,
        namespace,
        updated_values,
        ret.get("new_state"),
    )

    # Delete helm_release with test flag
    ret = await hub.states.helm.release.absent(
        test_ctx,
        name=helm_release_temp_name,
        resource_id=resource_id,
        namespace=namespace,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete helm.release '{helm_release_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert_release(helm_release_temp_name, chart, namespace, updated_values, resource)

    # Delete helm_release with real account
    ret = await hub.states.helm.release.absent(
        ctx,
        name=helm_release_temp_name,
        resource_id=resource_id,
        namespace=namespace,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted helm.release '{helm_release_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert_release(helm_release_temp_name, chart, namespace, updated_values, resource)

    # Deleting helm_release again should be an no-op
    ret = await hub.states.helm.release.absent(
        ctx, name=helm_release_temp_name, resource_id=resource_id, namespace=namespace
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"helm.release '{helm_release_temp_name}' already absent" in ret["comment"]


def assert_release(
    helm_release_name, chart, namespace, values, resource, resource_id=None
):
    assert helm_release_name == resource.get("name")
    assert chart in resource.get("chart")
    assert namespace == resource.get("namespace")
    assert values == resource.get("values")
    if resource_id:
        assert resource_id == resource.get("resource_id")
    else:
        assert helm_release_name == resource.get("resource_id")
